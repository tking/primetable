module Primetable
  class Sieve
    attr_reader :list

    def initialize(total)
      @list = primes.take(total).to_a
    end

    def primes
      (2..(Float::INFINITY)).lazy.reject do |i|
        (2...i).any? { |j| (i % j) < 1 }
      end
    end
  end
end
