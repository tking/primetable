module Primetable
  class Cell
    attr_accessor :data, :row, :width

    def initialize(data, row)
      @row = row
      @data = data
      @width = row.cell_width
    end

    def contents
      "#{DIVIDER} #{data.to_s.gsub(/^1$/, ' ')}#{padding}#{terminating_divider}"
    end

    private

    def padding
      " " * (width - (data.to_s.size + 1))
    end

    def terminating_divider
      row.cells.last == self ? "|\n" : ""
    end
  end
end
