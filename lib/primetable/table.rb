module Primetable
  class Table
    attr_reader :rows, :cell_width
    attr_accessor :primes_array

    def initialize(primes_array)
      @primes_array = primes_array.unshift(1)
      @cell_width = calculate_cell_width
      @rows = calculated_primes.map { |row| Row.new(row, self) }
    end

    def print
      [header, body, footer].each { |i| printf i }
    end

    def header
      "#{footer}|#{header_text.center(row_width - 1)}|\n"
    end

    def body
      rows.map(&:contents).join
    end

    def footer
      border
    end

    def border
      "+#{cell_border * primes_array.count}\n"
    end

    private

    def cell_border
      LINE * cell_width + CORNER
    end

    def calculate_cell_width
      if primes_array.count  <= MIN_NUMBER_OF_PRIMES
        header_text.size / primes_array.size
      else
        calculated_primes.flatten.max.to_s.size + 2
      end
    end

    def calculated_primes
      primes_array.map do |n|
        primes_array.map { |nn| n * nn }
      end
    end

    def header_text
      "Prime Number Multiplication Table (#{primes_array.size - 1} numbers)"
    end

    def row_width
      (cell_width + 1) * rows.size
    end
  end
end
