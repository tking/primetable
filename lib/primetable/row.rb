module Primetable
  class Row
    attr_reader :cells, :cell_width
    attr_accessor :table

    def initialize(row_array, table)
      @table = table
      @cell_width = table.cell_width
      @cells = row_array.map { |data| Cell.new(data, self) }
    end

    def contents
      [[table.border], cells.map(&:contents)]
    end

    private

    def final_cell_terminator
      "|\n"
    end

  end
end
