require "primetable/version"
require "primetable/sieve"
require "primetable/table"
require "primetable/row"
require "primetable/cell"

module Primetable
  LINE = "-"
  CORNER = "+"
  DIVIDER = "|"
  MIN_NUMBER_OF_PRIMES = 7

  def self.output(total)
    primes = Primetable::Sieve.new(total).list
    table = Primetable::Table.new(primes).print
  end
end
