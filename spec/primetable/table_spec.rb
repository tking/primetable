describe Primetable::Table do
  let(:table) { Primetable::Table.new(primes_array) }
  let(:primes_array) { [2, 3, 5, 7] }

  subject { table }

  its(:primes_array) { should include(1) }
  its(:primes_array) { should have(5).items }

  describe ".cell_width (from primes_array)" do
    context "when the largest product contains 4 digits" do
      let(:primes_array) { [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37] }
      its(:cell_width) { should eql(6) }
    end

    context "with 8 elements" do
      let(:primes_array) { [2, 3, 5, 7, 11, 13, 17, 19] }
      its(:cell_width) { should eql(5) }
    end

    context "with #{Primetable::MIN_NUMBER_OF_PRIMES} elements" do
      let(:primes_array) { [2, 3, 5, 7, 11, 13, 17] }
      its(:cell_width) { should eql(5) }
    end

    context "with 6 elements" do
      let(:primes_array) { [2, 3, 5, 7, 11, 13] }
      its(:cell_width) { should eql(6) }
    end

    context "with 5 elements" do
      let(:primes_array) { [2, 3, 5, 7, 11] }
      its(:cell_width) { should eql(7) }
    end

    context "with 4 elements" do
      let(:primes_array) { [2, 3, 5, 7] }
      its(:cell_width) { should eql(9) }
    end

    context "with 3 elements" do
      let(:primes_array) { [2, 3, 5] }
      its(:cell_width) { should eql(11) }
    end

    context "with 2 elements" do
      let(:primes_array) { [2, 3] }
      its(:cell_width) { should eql(15) }
    end
  end

  describe "#border" do
    subject { table.border }

    context "with 2 elements in primes_array" do
      let(:primes_array) { [2, 3] }
      its(:length) { should eql(50) }
    end

    context "with 8 elements in primes_array" do
      let(:primes_array) { [2, 3, 5, 7, 11, 13, 17, 19] }
      its(:length) { should eql(56) }
    end
  end

  # integration test!
  its(:body) { should eql(TABLE_WITH_FOUR_PRIMES) }

  it "creates Primetable::Row objects for each table row" do
    expect(Primetable::Row).to receive(:new).exactly(5).times
    subject
  end
end
