require 'spec_helper'

describe Primetable::Cell do
  let(:cells) { double("Cells", last: nil) }
  let(:cell_width) { 5 }
  let(:row) { double("Row", cells: cells, cell_width: cell_width) }
  let(:data) { 2 }

  subject { Primetable::Cell.new(data, row) }

  its(:width) { should eql(cell_width) }
  its(:contents) { should eql(CELL_CONTENT) }

  describe "The last cell" do
    before { allow(cells).to receive(:last).and_return(subject) }
    its(:contents) { should eql(LAST_CELL_CONTENT) }
  end
end
