require 'spec_helper'

describe Primetable::Row do
  let(:cell_width) { 2 }
  let(:table) { double("Table", cell_width: cell_width, border: '') }
  let(:row_array) { [1, 2, 3, 5, 7] }

  subject { Primetable::Row.new(row_array, table) }

  its(:table) { should eql(table) }
  its(:cell_width) { should eql(cell_width) }
  its(:contents) { should eql(ROW_CONTENT) }

  it "creates cells for each prime number" do
    expect(Primetable::Cell).to receive(:new).exactly(row_array.count).times
    subject
  end
end
