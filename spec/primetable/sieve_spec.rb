require 'spec_helper'

describe Primetable::Sieve do
  subject { Primetable::Sieve.new(3) }

  its(:list) { should be_an(Array) }
  its(:list) { should_not include(4) }
end
