# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'primetable/version'

Gem::Specification.new do |spec|
  spec.name          = "primetable"
  spec.version       = Primetable::VERSION
  spec.authors       = ["Timothy King"]
  spec.email         = ["tmk@lordzork.com"]
  spec.summary       = %q{Generates a multiplication table of prime numbers}
  spec.homepage      = "https://gist.github.com/danvideo/8f722112c1dcc6ab4d5d"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.3"
  spec.add_development_dependency "rspec-its", "~> 1.2"
  spec.add_development_dependency "rspec-collection_matchers", "~> 1.1"
end
