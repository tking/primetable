# Primetable

Outputs a multiplication table of prime numbers. [See here for more information](https://gist.github.com/danvideo/8f722112c1dcc6ab4d5d).

## Installation

Clone the repository:

    $ git clone https://gitlab.com/tking/primetable.git

Change into the primetable directory and run bundle:

    $ cd primetable && bundle

## Usage

` bin/primetable <any number>`
